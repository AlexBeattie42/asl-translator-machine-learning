import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense, Activation, Input
from keras import applications
from keras.applications.vgg16 import VGG16
from keras import optimizers
from keras.layers import ActivityRegularization, BatchNormalization
from keras.regularizers import l1
import keras.backend as K
import tensorflow as tf
import math
from keras.utils import np_utils
import keras
import os
from keras.models import load_model
import ssl
import matplotlib.pyplot as plt
import itertools
from sklearn import datasets
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, KFold, GridSearchCV
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical
from keras.wrappers.scikit_learn import KerasClassifier
from keras import Model
import keras_metrics
import cv2

from tensorflow.python.client import device_lib
assert 'GPU' in str(device_lib.list_local_devices())
from keras import backend
assert len(backend.tensorflow_backend._get_available_gpus()) > 0

# enable use of gpu
config = tf.ConfigProto( device_count = {'GPU': 1, 'CPU': 10} ) 
sess = tf.Session(config=config) 
keras.backend.set_session(sess)

ssl._create_default_https_context = ssl._create_unverified_context

# set tensor ordering
K.set_image_dim_ordering('tf')


def train_model(img_width,img_height,complete_model_path,train_data_dir,nb_train_samples,epochs,batch_size):

    # used for loading data
    datagen = ImageDataGenerator(rescale=1. / 255)

    # used for loading training data from file
    # using generator helps memory consumption during triaining
    train_generator = datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=True)

    print(train_generator.class_indices)

    # build the VGG16 network
    # include_top=False loads model without fully-connected classifier on top
    input_tensor = Input(shape=(300, 300, 3))
    vgg16 = VGG16(include_top=False, weights='imagenet', input_tensor=input_tensor)

    # new top
    x = Flatten(name='flatten_conv_block_output')(vgg16.layers[-1].output)
    x = Dense(128, use_bias=True, name='fc1')(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = Dense(64, use_bias=True, name='fc2')(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = Dense(26, activation='softmax', name='prediction')(x)

    # add new classifier to vgg16
    new_model = Model(input=vgg16.input, output=x)
    new_model.summary()

    # freeze the conv blocks
    counter = 0
    for layer in new_model.layers:
        if counter < 19:
            layer.trainable = False
            counter+= 1

    # set optimizer
    sgd = optimizers.SGD(lr=0.01, decay=0.0001, momentum=0.9, nesterov=True)

    # compile model
    new_model.compile(optimizer=sgd, loss='mean_squared_error', metrics=['accuracy',keras_metrics.precision(),keras_metrics.recall()])

    # train
    history = new_model.fit_generator(train_generator,
              epochs=epochs,
              steps_per_epoch=nb_train_samples // batch_size,
              workers=6
              )

    # save model
    new_model.save(complete_model_path)

    # summarize history for accuracy
    plt.plot(history.history['accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.show()

    # summarize history for precision
    plt.plot(history.history['precision'])
    plt.title('model precision')
    plt.ylabel('precision')
    plt.xlabel('epoch')
    plt.show()

    # summarize history for recall
    plt.plot(history.history['recall'])
    plt.title('model recall')
    plt.ylabel('recall')
    plt.xlabel('epoch')
    plt.show()

if __name__ == "__main__":

    # set vars
    img_width, img_height = 300, 300
    complete_model_path = 'improved_model.h5'
    train_data_dir = 'curated_data/train'
    nb_train_samples = 2336
    epochs = 50
    batch_size = 32

    train_model(img_width,img_height,complete_model_path,train_data_dir,nb_train_samples,epochs,batch_size)