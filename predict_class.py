import keras
from keras.models import load_model
import cv2
import numpy as np


def predict_subimages(images, model_path):


    model = load_model(model_path)

    # comes from datagen.class_indicies during training
    class_order = ['D', 'B', 'K', 'R', 'X', 'L', 'M', 'Q', 'Y', 'S', 'F', 'I', 'N', 'U', 'P', 'O', 'H', 'W', 'V', 'J', 'A', 'Z', 'E', 'T', 'G','C']

    predictions = []
    for image in images:

        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) 
        image = np.expand_dims(image, axis=0)
        tmp_prediction = model.predict(image)
        tmp_prediction = np.argmax(tmp_prediction)
        letter = class_order[tmp_prediction]
        predictions.append(letter)

    return predictions