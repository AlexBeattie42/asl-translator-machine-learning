import cv2
import tensorflow as tf
import datetime
import numpy as np


# load frozen inference graph
# input: path to checkpoint file
# output: loaded graph object and current tensorflow session
def load_inference_graph(PATH_TO_CKPT):

    # initialize graph
    detection_graph = tf.Graph()

    # use this graph as the default
    with detection_graph.as_default():

        # initialize graph definition
        od_graph_def = tf.GraphDef()

        # read graph definition from checkpoint file and load that definition into the default graph
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        # set the session's graph
        sess = tf.Session(graph=detection_graph)

    return detection_graph, sess

# get confidence scores and bounding boxes for an image
# input: image as numpy array, graph, and current tensorflow session
# output: bouding boxes and confidence scores
def detect_objects(image, detection_graph, sess):

    # get tensors from the graph
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # flatten image
    image_expanded = np.expand_dims(image, axis=0)

    # feed image through graph and get predictions
    (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores,detection_classes, num_detections], feed_dict={image_tensor: image_expanded})

    return np.squeeze(boxes), np.squeeze(scores)

# draw  bounding boxes on the image
def draw_box_on_image(num_hands_detect, score_thresh, scores, boxes, im_width, im_height, image):

    for i in range(num_hands_detect):
        if (scores[i] > score_thresh):
            (left, right, top, bottom) = (boxes[i][1] * im_width, boxes[i][3] * im_width, boxes[i][0] * im_height, boxes[i][2] * im_height)
            p1 = (int(left)-20, int(top)-20)
            p2 = (int(right)+20, int(bottom)+20)
            cv2.rectangle(image, p1, p2, (77, 255, 9), 1)


# get the highest confidence bounding boxes for each hand
def filter_and_pad_boxes(num_hands_detect, score_thresh, scores, boxes, im_width, im_height):

    bounding_boxes = []
    for i in range(num_hands_detect):
        if (scores[i] > score_thresh):
            (left, right, top, bottom) = (boxes[i][1] * im_width, boxes[i][3] * im_width, boxes[i][0] * im_height, boxes[i][2] * im_height)
            bounding_boxes.append([int(left-20),int(top-20),int(right+20),int(bottom+20)])
    
    return bounding_boxes

# input: path to image
# output: bounding boxes of detected hands in the image and cropped subimage
# output format: list of lists as [x0,y0,x1,y1] i.e. [left,top,right,bottom]
def get_bounding_boxes(image_path):

    # path to tensorflow graph
    PATH_TO_CKPT = r"hand_inference_graph\\frozen_inference_graph.pb"

    # load in frozen inference graph
    detection_graph, sess = load_inference_graph(PATH_TO_CKPT)

    # load image
    image = cv2.imread(image_path)
    height, width, _ = image.shape

    # convert opencv BGR image format to RGB for tensorflow 
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # get predicted bounding boxes and confidence scores in each
    boxes, scores = detect_objects(image, detection_graph, sess)

    # max number of hands to detect
    num_hands_detect = 1

    # get best score boxes and increase area of boxes
    bouding_boxes = filter_and_pad_boxes(num_hands_detect, 0.2, scores, boxes, width, height)

    sub_images = []
    for (x0, y0, x1, y1) in bouding_boxes:
        sub_images.append(cv2.cvtColor(image[y0:y1,x0:x1], cv2.COLOR_RGB2BGR))

    ## uncomment this section of code to get a visualization of the bounding boxes on the image
    # draw_box_on_image(num_hands_detect, 0.2, scores, boxes, width, height, image)
    # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    # cv2.imshow('iamge',image)
    # cv2.waitKey(10000)

    return bouding_boxes, sub_images
    
if __name__ == "__main__":

    # source image
    image_path = r"1.jpg"

    boxes, sub_images = get_bounding_boxes(image_path)
